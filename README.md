# prueba-regcheq

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
## Usage
Use the user admin@mail.com with password admin123 to access to the admin module for CRUD of products and categories

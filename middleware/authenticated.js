import jwtDecode from 'jwt-decode'
export default async function ({ app, redirect }) {
  await app.$axios.get(app.$axios.defaults.baseURL + 'auth/profile', { headers: { Authorization: 'Bearer ' + app.$cookies.get('access_token') } }).then(() => {
    const data = jwtDecode(app.$cookies.get('access_token'))
    const now = new Date() / 1000
    if (data.exp < now) {
      app.$axios.post(app.$axios.defaults.baseURL + 'auth/refresh-token', { refreshToken: app.$cookies.get('refresh_token') }).then((result) => {
        app.$cookies.remove('access_token')
        app.$cookies.remove('refresh_token')
        app.$cookies.set('access_token', result.data.access_token, {
          path: '/',
          maxAge: 60 * 60 * 24 * 20
        })
        app.$cookies.set('refresh_token', result.data.refresh_token, {
          path: '/',
          maxAge: 60 * 60 * 10
        })
      })
    }
  }).catch(() => {
    app.$cookies.remove('access_token')
    app.$cookies.remove('refresh_token')
    return redirect('/' + (app.$cookies.get('language') === 'es' ? '' : app.$cookies.get('language')) + '/')
  })
}

// import Cookies from 'js-cookie'
export default async function ({ app, redirect }) {
  await app.$axios.get(app.$axios.defaults.baseURL + 'auth/profile', { headers: { Authorization: 'Bearer ' + app.$cookies.get('access_token') } }).then((result) => {
    if (result.data.role !== 'admin') {
      app.$cookies.remove('access_token')
      app.$cookies.remove('refresh_token')
      return redirect('/' + (app.$cookies.get('language') === 'es' ? '' : app.$cookies.get('language')) + '/')
    }
  }).catch(() => {
    app.$cookies.remove('access_token')
    app.$cookies.remove('refresh_token')
    return redirect('/' + (app.$cookies.get('language') === 'es' ? '' : app.$cookies.get('language')) + '/')
  })
}

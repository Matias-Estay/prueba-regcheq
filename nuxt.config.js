import { sortRoutes } from '@nuxt/utils'
export default {
  generate: {
    fallback: true
  },
  target: 'static',
  // Global page headers: https://go.nuxtjs.dev/config-head+
  head: {
    title: 'prueba-regcheq',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/assets/app.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build'
  ],

  router: {
    mode: 'history',
    base: '/',
    extendRoutes (routes, resolve) {
      routes.splice((routes.findIndex((x) => { return x.path === '/login' })), 1)
      routes.push({
        name: 'login___es',
        path: '/',
        components: {
          default: resolve(__dirname, 'pages/login')
        }
      })
      routes.push({
        name: 'login___pr',
        path: '/pr/',
        components: {
          default: resolve(__dirname, 'pages/login')
        }
      })
      routes.push({
        name: 'login___en',
        path: '/en/',
        components: {
          default: resolve(__dirname, 'pages/login')
        }
      })
      sortRoutes(routes)
    }
  },
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    '@nuxtjs/i18n',
    'cookie-universal-nuxt',
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios'
  ],
  i18n: {
    locales: [
      {
        code: 'en',
        name: 'English',
        file: 'en-US.js'
      },
      {
        code: 'es',
        name: 'Español',
        file: 'es-ES.js'
      },
      {
        code: 'pr',
        name: 'Portugues',
        file: 'pr-PR.js'
      }
    ],
    lazy: true,
    langDir: 'lang/',
    defaultLocale: 'es'
  },

  bootstrapVue: {
    icons: true
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: 'https://api.escuelajs.co/api/v1/'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}

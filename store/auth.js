const state = {
}

const mutations = {
}

const actions = {
  async Login ({commit}, user) {
    return await this.$axios.post(this.$axios.defaults.baseURL + 'auth/login', { email: user.email, password: user.password }).then((result) => {
      this.$cookies.set('access_token', result.data.access_token, {
        path: '/',
        maxAge: 60 * 60 * 24 * 20
      })
      this.$cookies.set('refresh_token', result.data.refresh_token, {
        path: '/',
        maxAge: 60 * 60 * 10
      })
      this.$axios.get(this.$axios.defaults.baseURL + 'auth/profile', { headers: { Authorization: 'Bearer ' + this.$cookies.get('access_token') } }).then((result2) => {
        this.$cookies.set('rol', result2.data.role, {
          path: '/',
          maxAge: 60 * 60 * 24 * 20
        })
        this.$cookies.set('avatar', result2.data.avatar, {
          path: '/',
          maxAge: 60 * 60 * 10
        })
        this.$cookies.set('name', result2.data.name, {
          path: '/',
          maxAge: 60 * 60 * 10
        })
        this.$router.push('/' + (this.$cookies.get('language') === 'es' ? '' : this.$cookies.get('language') + '/') + 'home')
      }).catch((error) => {
        return error
      })
    }).catch((error) => {
      return error
    })
  },
  logout ({ commit }) {
    this.$cookies.remove('access_token')
    this.$cookies.remove('refresh_token')
    this.$router.push('/' + (this.$cookies.get('language') === 'es' ? '' : this.$cookies.get('language')) )
  }
}

const getters = {
  Getpokemons: (state) => {
    return state.pokemons
  },
  Getpokemon: (state) => {
    return state.pokemon
  }
}


export const auth = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

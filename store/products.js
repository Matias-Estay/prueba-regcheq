const state = {
  products: []
}

const mutations = {
  Setproducts (state, products) {
    state.products = products
  }
}

const actions = {
  async Get_products ({ commit }) {
    return await this.$axios.get(this.$axios.defaults.baseURL + 'products/').then((result) => {
      commit('Setproducts', result.data)
    }).catch((error) => {
      return error
    })
  },
  async Update_product ({ commit }, data) {
    return await this.$axios.put(this.$axios.defaults.baseURL + 'products/' + data.id, { title: data.title, price: data.price, description: data.description, images: data.images, category: data.category }).then((result) => {
      return 200
    }).catch((error) => {
      return error
    })
  },
  async Delete_product ({ commit }, data) {
    return await this.$axios.delete(this.$axios.defaults.baseURL + 'products/' + String(data.id), {}).then((result) => {
      return result
    }).catch((error) => {
      return error
    })
  },
  async Create_product ({ commit }, data) {
    return await this.$axios.post(this.$axios.defaults.baseURL + 'products/', { title: data.title, price: data.price, description: data.description, images: data.images, categoryId: data.categoryId}).then((result) => {
      return 201
    }).catch((error) => {
      return error
    })
  },
  async Filter_products ({ commit }, data) {
    const serializeQuery = (data) => {
      return Object.keys(data).map( (key) => `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`).join('&')}
    const url = '?' + serializeQuery(data)
    return await this.$axios.get(this.$axios.defaults.baseURL + 'products/' + url, {}).then((result) => {
      commit('Setproducts', result.data)
    }).catch((error) => {
      return error
    })
  },
}

const getters = {
  Getproducts: (state) => {
    return state.products
  }
}

export const products = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

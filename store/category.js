const state = {
  categories: []
}

const mutations = {
  Setcategories (state, categories) {
    state.categories = categories
  }
}

const actions = {
  async Get_All_categories ({ commit }) {
    return await this.$axios.get(this.$axios.defaults.baseURL + 'categories').then((result) => {
      commit('Setcategories', result.data)
    }).catch((error) => {
      return error
    })
  },
  async Update_category ({ commit }, data) {
    return await this.$axios.put(this.$axios.defaults.baseURL + 'categories/' + data.id, { name: data.name, image: data.image }).then(() => {
      return 200
    }).catch((error) => {
      return error
    })
  },
  async Delete_category ({ commit }, data) {
    return await this.$axios.delete(this.$axios.defaults.baseURL + 'categories/' + data.id).then(() => {
      return 200
    }).catch((error) => {
      return error
    })
  },
  async Create_category ({ commit }, data) {
    return await this.$axios.post(this.$axios.defaults.baseURL + 'categories/', { name: data.name, image: data.image }).then(() => {
      return 201
    }).catch((error) => {
      return error
    })
  }
}

const getters = {
  Getcategories: (state) => {
    return state.categories
  }
}

export const category = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}

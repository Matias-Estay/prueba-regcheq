import { auth } from './auth.js'
import { products } from './products.js'
import { category } from './category'
export const modules = {
  auth,
  products,
  category
}

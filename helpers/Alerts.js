const Swal = require('sweetalert2')
const Alerts = {
  error: function (message) {
    Swal.fire(
      'Error!',
      message,
      'error'
    )
  },
  success: function (message) {
    Swal.fire(
      'Success!',
      message,
      'success'
    )
  },
  question: function (message, cancel_text, confirm_text) {
    return Swal.fire({
      title: message,
      showCancelButton: true,
      confirmButtonText: confirm_text,
      confirmButtonColor: 'red',
      denyButtonText: cancel_text
    }).then( ( result ) => {
      if (result.isConfirmed) {
        return 'OK'
      } else if (result.isDenied) {
        return 'NO'
      }
    })
  }

}

export default Alerts
